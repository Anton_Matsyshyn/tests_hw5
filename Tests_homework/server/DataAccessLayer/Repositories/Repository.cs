﻿using DataAccessLayer.Repositories.Interfaces;
using DataAccessLayer.UnitOfWork.Interfaces;
using DataAccessLayer.Models.Abstractions;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace DataAccessLayer.Repositories
{
    public class Repository<T> : IRepository<T>
        where T : BaseEntity
    {
        private readonly HomeworkDbContext _context;
        public Repository(HomeworkDbContext context, IUnitOfWork unitOfWork)
        {
            _context = context;
            this.unitOfWork = unitOfWork;
        }

        public virtual IUnitOfWork unitOfWork { get; }

        public virtual void Create(T entity)
        {
            entity.Id = 0;
            _context.Add(entity);
        }

        public virtual void Delete(int id)
        {
            var entity = _context.Set<T>().Find(id);
            _context.Remove(entity);
        }

        public virtual IQueryable<T> Get()
        {
            return _context.Set<T>().AsQueryable();
        }

        public virtual T Get(int id)
        {
            return _context.Set<T>().Find(id);
        }

        public virtual void Update(T updatedEntity)
        {
            _context.Entry(updatedEntity).State = EntityState.Modified;
        }
    }
}
