﻿using DataAccessLayer.UnitOfWork.Interfaces;
using System.Linq;

namespace DataAccessLayer.Repositories.Interfaces
{
    public interface IRepository<T> 
        where T:class
    {
        IUnitOfWork unitOfWork { get; }
        IQueryable<T> Get();
        T Get(int id);
        void Create(T entity);
        void Update(T updatedEntity);
        void Delete(int id);
    }
}
