﻿using AutoMapper;
using BusinessLogicLayer.Exceptions;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Services.Abstractions;
using Common.DTO;
using DataAccessLayer.Repositories.Interfaces;
using DataAccessLayer.Models;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace BusinessLogicLayer.Services
{
    public class UserService : BaseService, IUserService
    {
        private readonly IRepository<User> _userRepository;
        private readonly IRepository<TaskState> _taskStateRepository;

        public UserService(IRepository<User> userRepository,
                           IRepository<TaskState> taskStateRepository,
                           IMapper mapper) : base(mapper)
        {
            _userRepository = userRepository;
            _taskStateRepository = taskStateRepository;
        }
        public int CreateUser(UserDTO userDto)
        {
            if (userDto == null)
                throw new NullEntityException(typeof(UserDTO));

            var user = _mapper.Map<User>(userDto);

            _userRepository.Create(user);
            _userRepository.unitOfWork.SaveChanges();

            userDto.Id = user.Id;

            return userDto.Id;
        }

        public void DeleteUser(int userId)
        {
            if (GetUser(userId) == null)
                throw new NotFoundException(typeof(User), userId);

            _userRepository.Delete(userId);
            _userRepository.unitOfWork.SaveChanges();
        }

        public IEnumerable<UserDTO> GetAllUsers()
        {
            return _userRepository.Get().Select(u => _mapper.Map<UserDTO>(u));
        }

        public UserDTO GetUser(int userId)
        {
            User entity = _userRepository.Get(userId);

            if (entity == null)
                throw new NotFoundException(typeof(User), userId);

            var dto = _mapper.Map<UserDTO>(entity);
            return dto;
        }

        public IEnumerable<TaskDTO> GetUserUnfinishedTasks(int userId)
        {
            if (GetUser(userId) == null)
                throw new NotFoundException(typeof(User), userId);

            var finishedTaskStatus = _taskStateRepository.Get().FirstOrDefault(t => t.State == "Finished");

            var tasks = _userRepository.Get().Where(u => u.Id == userId)
                                             .Include(u => u.Tasks)
                                             .SelectMany(u => u.Tasks)
                                             .Where(t => t.TaskStateId != finishedTaskStatus.Id)
                                             .Select(t => _mapper.Map<TaskDTO>(t))
                                             .ToArray();

            return tasks;
        }

        public void UpdateUser(UserDTO userDto)
        {
            if (userDto == null)
                throw new NullEntityException(typeof(UserDTO));

            if (GetUser(userDto.Id) == null)
                throw new NotFoundException(typeof(User), userDto.Id);

            var userEntity = _userRepository.Get(userDto.Id);
            userEntity = _mapper.Map(userDto, userEntity);

            _userRepository.Update(userEntity);
            _userRepository.unitOfWork.SaveChanges();
        }
    }
}
