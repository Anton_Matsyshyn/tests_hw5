﻿using AutoMapper;
using BusinessLogicLayer.Exceptions;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Services.Abstractions;
using Common.DTO;
using DataAccessLayer.Repositories.Interfaces;
using DataAccessLayer.Models;
using System.Collections.Generic;
using System.Linq;

namespace BusinessLogicLayer.Services
{
    public class TaskService : BaseService, ITaskService
    {
        private readonly IRepository<Task> _taskRepository;
        private readonly IRepository<TaskState> _taskStateRepository;
        public TaskService(IRepository<Task> taskRepository,
                           IRepository<TaskState> taskStateRepository,
                           IMapper mapper) : base(mapper)
        {
            _taskRepository = taskRepository;
            _taskStateRepository = taskStateRepository;
        }

        public int CreateTask(TaskDTO taskDto)
        {
            if (taskDto == null)
                throw new NullEntityException(typeof(TaskDTO));

            var task = _mapper.Map<Task>(taskDto);

            _taskRepository.Create(task);
            _taskRepository.unitOfWork.SaveChanges();

            taskDto.Id = task.Id;

            return taskDto.Id;
        }

        public void DeleteTask(int taskId)
        {
            if (GetTask(taskId) == null)
                throw new NotFoundException(typeof(Task), taskId);

            _taskRepository.Delete(taskId);

            _taskRepository.unitOfWork.SaveChanges();
        }

        public void FinishTask(int taskId)
        {
            var task = GetTask(taskId);

            var finishedTaskState = _taskStateRepository.Get().FirstOrDefault(t => t.State == "Finished");

            task.TaskStateId = finishedTaskState.Id;

            UpdateTask(_mapper.Map<TaskDTO>(task));
        }

        public IEnumerable<TaskDTO> GetAllTask()
        {
            return _taskRepository.Get().Select(t => _mapper.Map<TaskDTO>(t));
        }

        public TaskDTO GetTask(int taskId)
        {
            Task entity = _taskRepository.Get(taskId);

            if (entity == null)
                throw new NotFoundException(typeof(Task), taskId);

            var dto = _mapper.Map<TaskDTO>(entity);
            return dto;
        }

        public void UpdateTask(TaskDTO task)
        {
            if (task == null)
                throw new NullEntityException(typeof(TaskDTO));

            if (GetTask(task.Id) == null)
                throw new NotFoundException(typeof(Task), task.Id);

            var taskEntity = _taskRepository.Get(task.Id);
            taskEntity = _mapper.Map(task, taskEntity);

            _taskRepository.Update(taskEntity);
            _taskRepository.unitOfWork.SaveChanges();
        }
    }
}
