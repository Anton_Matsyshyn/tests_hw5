﻿using AutoMapper;
using BusinessLogicLayer.Exceptions;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Services.Abstractions;
using Common.DTO;
using DataAccessLayer.Repositories.Interfaces;
using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace BusinessLogicLayer.Services
{
    public class CollectionService:BaseService, ICollectionService
    {
        private readonly IRepository<Project> _projectsRep;
        private readonly IRepository<Task> _tasksRep;
        private readonly IRepository<TaskState> _taskStatesRep;
        private readonly IRepository<Team> _teamsRep;
        private readonly IRepository<User> _usersRep;

        public CollectionService(IRepository<Project> projectRep,
                                 IRepository<Task> taskRep,
                                 IRepository<TaskState> taskStates,
                                 IRepository<Team> teamRep,
                                 IRepository<User> userRep,
                                 IMapper mapper) :base(mapper)
        {
            _projectsRep = projectRep;
            _tasksRep = taskRep;
            _taskStatesRep = taskStates;
            _teamsRep = teamRep;
            _usersRep = userRep;
        }

        /// <summary>
        /// This is solution for 1st task from LINQ homework
        /// </summary>
        public Dictionary<int, int> GetProjectsIdAndTaskCount(int projectOwnerId)
        {
            if (_usersRep.Get().SingleOrDefault(u => u.Id == projectOwnerId) == null)
                throw new NotFoundException(typeof(User), projectOwnerId);

            var result = _projectsRep.Get().Where(p => p.AuthorId == projectOwnerId)
                                     .Include(p => p.Tasks)
                                     .ToDictionary(p => p.Id, p => p.Tasks.Count());

            return result;
        }

        /// <summary>
        /// This is solution for 2nd task from LINQ homework
        /// </summary>
        public IEnumerable<TaskDTO> GetUsersTask(int userId)
        {
            if(_usersRep.Get().SingleOrDefault(u => u.Id == userId) == null)
                throw new NotFoundException(typeof(User), userId);

            var result = _tasksRep.Get().Where(t => t.PerformerId == userId &&
                                               t.Name.Length < 45)
                                        .Select(t => _mapper.Map<TaskDTO>(t));
            return result;
        }

        /// <summary>
        /// This is solution for 3d task from LINQ homework
        /// </summary>
        public IEnumerable<TaskIdNameDTO> GetFinishedUserTasks(int userId)
        {
            if (_usersRep.Get().SingleOrDefault(u => u.Id == userId) == null)
                throw new NotFoundException(typeof(User), userId);

            var filteredTasks = _tasksRep.Get().Include(t => t.TaskState)
                                         .Where(t => t.PerformerId == userId &&
                                                t.FinishedAt.Year == 2020 &&
                                                t.TaskState.State == "Finished")
                                         .Select(t => new TaskIdNameDTO { Id = t.Id, Name = t.Name });

            return filteredTasks;
        }

        /// <summary>
        /// This is solution for 4th task from LINQ homework
        /// </summary>
        public IEnumerable<TeamWithUsersDTO> GetTeamsWithUsers()
        {
            var result = _teamsRep.Get().Include(t => t.Users)
                                        .Where(t => t.Users.Any(u => 2020 - u.Birthday.Year > 10))
                                        .Select(t => new TeamWithUsersDTO
                                        {
                                            TeamId = t.Id,
                                            TeamName = t.Name,
                                            Members = t.Users.Where(u => 2020 - u.Birthday.Year > 10)
                                                                    .OrderByDescending(u => u.RegisteredAt)
                                                                    .Select(u => _mapper.Map<UserDTO>(u))
                                        });

            return result;
        }

        /// <summary>
        /// This is solution for 5th task from LINQ homework
        /// </summary>
        public IEnumerable<UserWithTasks> GetUsersWithTasks()
        {
            var result = _usersRep.Get().OrderBy(u => u.FirstName)
                                        .Include(u => u.Tasks)
                                        .Select(u => new UserWithTasks
                                        {
                                            User = _mapper.Map<UserDTO>(u),
                                            Tasks = u.Tasks.OrderByDescending(t => t.Name.Length).Select(t => _mapper.Map<TaskDTO>(t))
                                        });

            return result;
        }

        /// <summary>
        /// This is solution for 6th task from LINQ homework
        /// </summary>
        public LastProjectAndTaskInfoDTO GetLastUserProject(int userId)
        {
            if (_usersRep.Get().SingleOrDefault(u => u.Id == userId) == null)
                throw new NotFoundException(typeof(User), userId);

            var result = _usersRep.Get().Include(u => u.Projects)
                                        .Include(u => u.Tasks)
                                        .Where(u => u.Id == userId)
                                        .Select(u => new
                                        {
                                            Author = _mapper.Map<UserDTO>(u),
                                            LastProject = _mapper.Map<ProjectDTO>(u.Projects.OrderBy(p => p.CreatedAt).LastOrDefault()),

                                            TaskCount = u.Projects.OrderBy(p => p.CreatedAt).LastOrDefault() == null ? 0 : u.Projects.OrderBy(p => p.CreatedAt).LastOrDefault().Tasks.Count(),
                                            UnfinishedOrCanceledTaskCount = u.Tasks.Count(t => t.TaskStateId == _taskStatesRep.Get().FirstOrDefault(ts => ts.State == "Finished").Id),
                                            Tasks = u.Tasks.ToList()
                                        })
                                        .ToList()
                                        .Select(pi => new LastProjectAndTaskInfoDTO
                                        {
                                            Author = pi.Author,
                                            LastProject = pi.LastProject,

                                            TaskCount = pi.TaskCount,
                                            UnfinishedOrCanceledTaskCount = pi.UnfinishedOrCanceledTaskCount,
                                            LongestTask = _mapper.Map<TaskDTO>(pi.Tasks.OrderBy(t => t.FinishedAt - t.CreatedAt).LastOrDefault())
                                        })
                                        .FirstOrDefault();

            return result;
        }

        /// <summary>
        /// This is solution for 7th task from LINQ homework
        /// </summary>
        public IEnumerable<ProjectAndTeamInfoDTO> GetProjectWithTeam()
        {
            var result = _projectsRep.Get().Include(p => p.Tasks)
                                                .ThenInclude(t => t.Performer)
                                           .Include(p => p.Team)
                                           .Include(p => p.Author)
                                           .Select(p => new ProjectAndTeamInfoDTO
                                           {
                                               Project = _mapper.Map<ProjectDTO>(p),
                                               LongestTask = _mapper.Map<TaskDTO>(p.Tasks.OrderBy(t => t.Description.Length).LastOrDefault()),
                                               ShortestTask = _mapper.Map<TaskDTO>(p.Tasks.OrderBy(t => t.Name.Length).FirstOrDefault()),
                                               TeammatesCount = p.Description.Length > 20 || p.Tasks.Count() < 3 ? p.Team.Users.Count() : default
                                           });

            return result;
        }
    }
}
