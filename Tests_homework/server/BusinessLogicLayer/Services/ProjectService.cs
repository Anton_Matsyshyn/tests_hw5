﻿using AutoMapper;
using BusinessLogicLayer.Exceptions;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Services.Abstractions;
using Common.DTO;
using DataAccessLayer.Repositories.Interfaces;
using DataAccessLayer.Models;
using System.Collections.Generic;
using System.Linq;

namespace BusinessLogicLayer.Services
{
    public class ProjectService : BaseService, IProjectService
    {
        private readonly IRepository<Project> _projectRepository;
        public ProjectService(IRepository<Project> projectRepository,
                              IMapper mapper) : base(mapper)
        {
            _projectRepository = projectRepository;
        }
        public int CreateProject(ProjectDTO projectDto)
        {
            if (projectDto == null)
                throw new NullEntityException(typeof(ProjectDTO));

            var project = _mapper.Map<Project>(projectDto);

            _projectRepository.Create(project);
            _projectRepository.unitOfWork.SaveChanges();

            projectDto.Id = project.Id;

            return projectDto.Id;
        }

        public void DeleteProject(int projectId)
        {
            if (GetProject(projectId) == null)
                throw new NotFoundException(typeof(Project), projectId);

            _projectRepository.Delete(projectId);

            _projectRepository.unitOfWork.SaveChanges();
        }

        public IEnumerable<ProjectDTO> GetAllProjects()
        {
            return _projectRepository.Get().Select(p => _mapper.Map<ProjectDTO>(p));
        }

        public ProjectDTO GetProject(int projectId)
        {
            Project entity = _projectRepository.Get(projectId);

            if (entity == null)
                throw new NotFoundException(typeof(Project), projectId);

            var dto = _mapper.Map<ProjectDTO>(entity);
            return dto;
        }

        public void UpdateProject(ProjectDTO projectDto)
        {
            if (projectDto == null)
                throw new NullEntityException(typeof(ProjectDTO));

            if (GetProject(projectDto.Id) == null)
                throw new NotFoundException(typeof(Project), projectDto.Id);

            var projectEntity = _projectRepository.Get(projectDto.Id);
            projectEntity = _mapper.Map(projectDto, projectEntity);

            _projectRepository.Update(projectEntity);
            _projectRepository.unitOfWork.SaveChanges();
        }
    }
}
