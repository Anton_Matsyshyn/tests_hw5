﻿using AutoMapper;
using BusinessLogicLayer.Exceptions;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Services.Abstractions;
using Common.DTO;
using DataAccessLayer.Repositories.Interfaces;
using DataAccessLayer.Models;
using System.Collections.Generic;
using System.Linq;

namespace BusinessLogicLayer.Services
{
    public class TeamService : BaseService, ITeamService
    {
        private readonly IRepository<Team> _repository;

        public TeamService(IRepository<Team> repository,
                           IMapper mapper) : base(mapper)
        {
            _repository = repository;
        }

        public int CreateTeam(TeamDTO teamDto)
        {
            if (teamDto == null)
                throw new NullEntityException(typeof(TeamDTO));

            var team = _mapper.Map<Team>(teamDto);

            _repository.Create(team);
            _repository.unitOfWork.SaveChanges();

            teamDto.Id = team.Id;

            return teamDto.Id;
        }

        public void DeleteTeam(int teamId)
        {
            if (GetTeam(teamId) == null)
                throw new NotFoundException(typeof(Team), teamId);

            _repository.Delete(teamId);
            _repository.unitOfWork.SaveChanges();
        }

        public IEnumerable<TeamDTO> GetAllTeams()
        {
            return _repository.Get().Select(t => _mapper.Map<TeamDTO>(t));
        }

        public TeamDTO GetTeam(int teamId)
        {
            Team entity = _repository.Get(teamId);

            if (entity == null)
                throw new NotFoundException(typeof(Team), teamId);

            var dto = _mapper.Map<TeamDTO>(entity);
            return dto;
        }

        public void UpdateTeam(TeamDTO team)
        {
            if (team == null)
                throw new NullEntityException(typeof(TeamDTO));

            if (GetTeam(team.Id) == null)
                throw new NotFoundException(typeof(Team), team.Id);

            var teamEntity = _repository.Get(team.Id);
            teamEntity = _mapper.Map(team, teamEntity);

            _repository.Update(teamEntity);
            _repository.unitOfWork.SaveChanges();
        }
    }
}
