﻿using Common.DTO;
using System.Collections.Generic;

namespace BusinessLogicLayer.Interfaces
{
    public interface IUserService
    {
        IEnumerable<UserDTO> GetAllUsers();
        UserDTO GetUser(int userId);

        int CreateUser(UserDTO user);
        void UpdateUser(UserDTO user);
        void DeleteUser(int userId);

        IEnumerable<TaskDTO> GetUserUnfinishedTasks(int userId);
    }
}
