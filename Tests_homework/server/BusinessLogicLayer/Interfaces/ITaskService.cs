﻿using Common.DTO;
using System.Collections.Generic;

namespace BusinessLogicLayer.Interfaces
{
    public interface ITaskService
    {
        IEnumerable<TaskDTO> GetAllTask();
        TaskDTO GetTask(int taskId);

        int CreateTask(TaskDTO task);
        void UpdateTask(TaskDTO task);
        void DeleteTask(int taskId);

        void FinishTask(int taskId);
    }
}
