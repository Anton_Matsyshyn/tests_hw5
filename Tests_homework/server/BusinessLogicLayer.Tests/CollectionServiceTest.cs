﻿using AutoMapper;
using BusinessLogicLayer.Exceptions;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Services;
using BusinessLogicLayer.Tests.Fakes;
using Common.DTO;
using DataAccessLayer.Repositories.Interfaces;
using DataAccessLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using DataAccessLayer.UnitOfWork;

namespace BusinessLogicLayer.Tests
{
    public class CollectionServiceTest
    {
        readonly ICollectionService _collectionService;

        readonly IRepository<Project> _projectsRep;
        readonly IRepository<Task> _tasksRep;
        readonly IRepository<TaskState> _taskStateRep;
        readonly IRepository<Team> _teamsRep;
        readonly IRepository<User> _usersRep;

        public CollectionServiceTest()
        {
            var context = new HomeworkDbContextFake();
            var unitOfWork = new UnitOfWork(context);

            _projectsRep = new RepositoryFake<Project>(context, unitOfWork);
            _tasksRep = new RepositoryFake<Task>(context, unitOfWork);
            _taskStateRep = new RepositoryFake<TaskState>(context, unitOfWork);
            _teamsRep = new RepositoryFake<Team>(context, unitOfWork);
            _usersRep = new RepositoryFake<User>(context, unitOfWork);

            _collectionService = CreateCollectionServiceInstanse();
        }

        /// <summary>
        /// 1st task from LINQ homework
        /// </summary>
        [Fact]
        public void GetProjectsIdAndTaskCount_WhereUserNotExist_NotFoundExceptionThrown()
        {
            Assert.Throws<NotFoundException>(() => _collectionService.GetProjectsIdAndTaskCount(0));
        }

        [Fact]
        public void GetProjectsIdAndTaskCount_WhenAddTwoTasks_ThanResult1And2()
        {
            var authorId = 10;

            var project = new Project 
            { 
                ProjectName = "Iron Man", 
                Description = "At least 15 characters...", 
                TeamId = 1, 
                AuthorId = authorId 
            };
            
            _projectsRep.Create(project);
            _projectsRep.unitOfWork.SaveChanges();

            var task1 = new Task 
            { 
                Name = "Cool armor",
                Description = "It has to be realy cool!", 
                TaskStateId = 1,
                PerformerId = 4,
                ProjectId = project.Id,
            };

            var task2 = new Task
            {
                Name = "Destructive weapon",
                Description = "A lot of very danger weapon",
                TaskStateId = 1,
                PerformerId = 9,
                ProjectId = project.Id,
            };

            _tasksRep.Create(task1);
            _tasksRep.Create(task2);
            _tasksRep.unitOfWork.SaveChanges();

            Assert.Equal(2, _collectionService.GetProjectsIdAndTaskCount(project.AuthorId.GetValueOrDefault())[project.Id]);
        }

        /// <summary>
        /// 2nd task from LINQ homework
        /// </summary>
        [Fact]
        public void GetUsersTask_WhenUserNotExist_NotFoundExceptionThrown()
        {
            Assert.Throws<NotFoundException>(() => _collectionService.GetUsersTask(0));
        }

        [Fact]
        public void GetUsersTask_WhenAddTwoTasks_ThanResultLength2()
        {
            var user = new User
            {
                FirstName = "Tony",
                LastName = "Stark",
                Email = "tonystark@starkindustry.com",
            };

            _usersRep.Create(user);
            _usersRep.unitOfWork.SaveChanges();

            var firstTask = new Task
            {
                Name = "1st finished task name",
                Description = "1st finished task description",
                PerformerId = user.Id,
                ProjectId = 2,
                TaskStateId = 3,
            };

            var secondTask = new Task
            {
                Name = "2nd finished task name",
                Description = "2nd finished task description",
                PerformerId = user.Id,
                ProjectId = 8,
                TaskStateId = 3,
            };

            _tasksRep.Create(firstTask);
            _tasksRep.Create(secondTask);
            _tasksRep.unitOfWork.SaveChanges();

            Assert.Equal(2, _collectionService.GetUsersTask(user.Id).Count());
        }
        
        [Fact]
        public void GetUsersTask_WhenAddTaskAndTaskWithNameMoreThan45Characters_ThanResultLength1()
        {
            var user = new User
            {
                FirstName = "Hulk",
                LastName = "Just Hulh",
                Email = "hulk@greendomain.com",
            };

            _usersRep.Create(user);
            _usersRep.unitOfWork.SaveChanges();

            var taskWithLengthLessThan45Characters = new Task
            {
                Name = "1st task name",
                Description = "1st task description",
                PerformerId = user.Id,
                ProjectId = 2,
                TaskStateId = 3,
            };

            var taskWithLengthMoreThan45Characters = new Task
            {
                Name = "2nd task name (more than 45 characters) :3333333333333333333",
                Description = "2nd task description ",
                PerformerId = user.Id,
                ProjectId = 8,
                TaskStateId = 2,
            };

            _tasksRep.Create(taskWithLengthLessThan45Characters);
            _tasksRep.Create(taskWithLengthMoreThan45Characters);
            _tasksRep.unitOfWork.SaveChanges();

            Assert.Single(_collectionService.GetUsersTask(user.Id));
        }

        /// <summary>
        /// 3d task from LINQ homework
        /// </summary>
        [Fact]
        public void GetFinishedUserTasks_WhenUserNotExist_NotFoundExceptionThrown()
        {
            Assert.Throws<NotFoundException>(() => _collectionService.GetFinishedUserTasks(0));
        }

        [Fact]
        public void GetFinishedUserTasks_WhenAdd2FinishedTaskAt2020_ThanResultLength2()
        {
            var user = new User
            {
                FirstName = "Anton",
                LastName = "Matsyshyn",
                Email = "anton.matsyshyn@outlook.com",
            };

            _usersRep.Create(user);
            _usersRep.unitOfWork.SaveChanges();

            var firstTask = new Task
            {
                Name = "1st task name",
                Description = "1st task description",
                PerformerId = user.Id,
                FinishedAt = DateTime.Parse("01/01/2020"),
                ProjectId = 2,
                TaskStateId = 3,
            };

            var secondTask = new Task
            {
                Name = "2nd task name",
                Description = "2nd task description",
                PerformerId = user.Id,
                FinishedAt = DateTime.Parse("01/01/2020"),
                ProjectId = 8,
                TaskStateId = 3,
            };

            _tasksRep.Create(firstTask);
            _tasksRep.Create(secondTask);
            _tasksRep.unitOfWork.SaveChanges();

            Assert.Equal(2, _collectionService.GetFinishedUserTasks(user.Id).Count());
        }
        
        [Fact]
        public void GetFinishedUserTasks_WhenAddUnfinishedTask_ThanResultLength0()
        {
            var user = new User
            {
                FirstName = "Anton",
                LastName = "Matsyshyn",
                Email = "anton.matsyshyn@outlook.com",
            };

            _usersRep.Create(user);
            _usersRep.unitOfWork.SaveChanges();

            var task = new Task
            {
                Name = "1st task name",
                Description = "1st task description",
                PerformerId = user.Id,
                FinishedAt = DateTime.Parse("01/01/2020"),
                ProjectId = 2,
                TaskStateId = 1,
            };

            _tasksRep.Create(task);
            _tasksRep.unitOfWork.SaveChanges();

            Assert.Empty(_collectionService.GetFinishedUserTasks(user.Id));
        }

        /// <summary>
        /// 4th task from LINQ homework
        /// </summary>
        [Fact]
        public void GetTeamsWithUsers_WhenAllUsersYoungerThan10_ThanTeamNotIncludesToCollection()
        {
            var team = new Team
            {
                Name = "party v minecrafte",
                CreatedAt = DateTime.Now
            };

            _teamsRep.Create(team);
            _teamsRep.unitOfWork.SaveChanges();

            var user1 = new User
            {
                FirstName = "Svetlana",
                LastName = "Diode",
                Email = "sveta-diode@mail.ru",
                Birthday = DateTime.Parse("01/01/2013"),
                TeamId = team.Id
            };

            var user2 = new User
            {
                FirstName = "Grisha",
                LastName = "MON-Transistor",
                Email = "transistory.ruliat@gmail.com",
                Birthday = DateTime.Parse("01/01/2015"),
                TeamId = team.Id
            };

            _usersRep.Create(user1);
            _usersRep.Create(user2);
            _usersRep.unitOfWork.SaveChanges();

            Assert.DoesNotContain(_collectionService.GetTeamsWithUsers(), t => t.TeamId == team.Id);
        }

        [Fact]
        public void GetTeamsWithUsers_WhenAddUserAtLeast10YearsOldAndUser_ThanResultMembersLength1()
        {
            var team = new Team
            {
                Name = "party v minecrafte",
                CreatedAt = DateTime.Now
            };

            _teamsRep.Create(team);
            _teamsRep.unitOfWork.SaveChanges();

            var user15YearsOld = new User
            {
                FirstName = "Anton",
                LastName = "Matsyshyn",
                Email = "anton.matsyshyn@outlook.com",
                Birthday = DateTime.Parse("01/01/2005"),
                TeamId = team.Id
            };

            var user = new User
            {
                FirstName = "Svetlana",
                LastName = "Diode",
                Email = "sveta-diode@mail.ru",
                Birthday = DateTime.Parse("01/01/2013"),
                TeamId = team.Id
            };

            _usersRep.Create(user);
            _usersRep.Create(user15YearsOld);
            _usersRep.unitOfWork.SaveChanges();

            Assert.Single(_collectionService.GetTeamsWithUsers().Single(t => t.TeamId == team.Id).Members);
        }

        /// <summary>
        /// 5th task from LINQ homework
        /// </summary>
        [Fact]
        public void GetUsersWithTasks_WhenAddUserAnd2Tasks_ThanResultTasksLength2()
        {
            var user = new User
            {
                FirstName = "Svetlana",
                LastName = "Diode",
                Email = "sveta-diode@mail.ru",
                Birthday = DateTime.Parse("01/01/2013"),
            };

            _usersRep.Create(user);
            _usersRep.unitOfWork.SaveChanges();

            var firstTask = new Task
            {
                Name = "1st task",
                Description = "1st task description",
                TaskStateId = 1,
                PerformerId = user.Id,
                ProjectId = 1,
            };

            var secondTask = new Task
            {
                Name = "2nd task",
                Description = "2nd task description",
                TaskStateId = 1,
                PerformerId = user.Id,
                ProjectId = 1,
            };

            _tasksRep.Create(firstTask);
            _tasksRep.Create(secondTask);
            _tasksRep.unitOfWork.SaveChanges();

            var tasks = _collectionService.GetUsersWithTasks().Single(t => t.User.Id == user.Id).Tasks.ToList();
            var orderedTasks = new List<TaskDTO>(tasks).OrderByDescending(t => t.Name.Length);

            Assert.Equal(2, tasks.Count());
            Assert.True(tasks.SequenceEqual(orderedTasks));
        }

        /// <summary>
        /// 6th task from LINQ homework
        /// </summary>
        [Fact]
        public void GetLastUserProject_WhenUserNotFount_ThanNotFoundExceptionThrown()
        {
            Assert.Throws<NotFoundException>(()=>_collectionService.GetLastUserProject(0));
        }

        [Fact]
        public void GetLastUserProject_ExpectedResult()
        {
            var user = new User
            {
                FirstName = "Svetlana",
                LastName = "Diode",
                Email = "sveta-diode@mail.ru",
                Birthday = DateTime.Parse("01/01/2013"),
            };

            _usersRep.Create(user);
            _usersRep.unitOfWork.SaveChanges();

            var project = new Project
            {
                ProjectName = "project name",
                Description = "project description (at least 15 characters...)",
                TeamId = 1,
                AuthorId = user.Id,
                CreatedAt = DateTime.Now
            };

            _projectsRep.Create(project);
            _projectsRep.unitOfWork.SaveChanges();

            //user tasks
            var task1 = new Task
            {
                Name = "1st task",
                Description = "1st task description",
                TaskStateId = 3,
                PerformerId = user.Id,
                ProjectId = 1,
            };

            var task2 = new Task
            {
                Name = "2nd task",
                Description = "2nd task description",
                TaskStateId = 3,
                PerformerId = user.Id,
                ProjectId = 1,
            };

            //project tasks
            var task3 = new Task
            {
                Name = "3d task",
                Description = "3d task description",
                TaskStateId = 2,
                PerformerId = 1,
                ProjectId = project.Id,
            };

            _tasksRep.Create(task1);
            _tasksRep.Create(task2);
            _tasksRep.Create(task3);
            _tasksRep.unitOfWork.SaveChanges();

            var result = _collectionService.GetLastUserProject(user.Id);

            Assert.Equal(user.Id, result.Author.Id);
            Assert.Equal(project.Id, result.LastProject.Id);
            Assert.Equal(1, result.TaskCount);
            Assert.Equal(2, result.UnfinishedOrCanceledTaskCount);
        }
        
        /// <summary>
        /// 7th task from LINQ homework
        /// </summary>
        [Fact]
        public void GetProjectWithTeam_ExpectedResult()
        {
            var team = new Team
            {
                Name = "test team"
            };

            _teamsRep.Create(team);
            _teamsRep.unitOfWork.SaveChanges();

            var user1 = new User
            {
                FirstName = "Svetlana",
                LastName = "Diode",
                Email = "sveta-diode@mail.ru",
                Birthday = DateTime.Parse("01/01/2013"),
                TeamId = team.Id,
            };

            var user2 = new User
            {
                FirstName = "Anton",
                LastName = "Matsyshyn",
                Email = "anton.matsyshyn@outlook.com",
                Birthday = DateTime.Parse("01/01/2005"),
                TeamId = team.Id
            };

            _usersRep.Create(user1);
            _usersRep.Create(user2);
            _usersRep.unitOfWork.SaveChanges();

            var project = new Project
            {
                ProjectName = "project name",
                Description = "project description (at least 15 characters...)",
                TeamId = team.Id,
                AuthorId = 1,
                CreatedAt = DateTime.Now
            };

            _projectsRep.Create(project);
            _projectsRep.unitOfWork.SaveChanges();

            var shortestTask = new Task
            {
                Name = ".",
                Description = "description",
                TaskStateId = 3,
                PerformerId = 1,
                ProjectId = project.Id,
            };

            var longestTask = new Task
            {
                Name = "Longest task name",
                Description = "Longest task description",
                TaskStateId = 3,
                PerformerId = 1,
                ProjectId = project.Id,
            };

            _tasksRep.Create(shortestTask);
            _tasksRep.Create(longestTask);
            _tasksRep.unitOfWork.SaveChanges();

            var result = _collectionService.GetProjectWithTeam().Single(r => r.Project.Id == project.Id);

            Assert.Equal(shortestTask.Id, result.ShortestTask.Id);
            Assert.Equal(longestTask.Id, result.LongestTask.Id);
            Assert.Equal(2, result.TeammatesCount);
        }
        private ICollectionService CreateCollectionServiceInstanse()
        {
            var collectionService = new CollectionService(_projectsRep,
                                                          _tasksRep,
                                                          _taskStateRep,
                                                          _teamsRep,
                                                          _usersRep,
                                                          new MapperConfiguration(cfg => 
                                                          {
                                                              cfg.CreateMap<ProjectDTO, Project>()
                                                                    .ForMember(p => p.ProjectName, options => options.MapFrom(dto => dto.Name));
                                                              cfg.CreateMap<Project, ProjectDTO>()
                                                                  .ForMember(dto => dto.Name, options => options.MapFrom(p => p.ProjectName));

                                                              cfg.CreateMap<Task, TaskDTO>();
                                                              cfg.CreateMap<TaskDTO, Task>();

                                                              cfg.CreateMap<Team, TeamDTO>();
                                                              cfg.CreateMap<TeamDTO, Team>();

                                                              cfg.CreateMap<UserDTO, User>();
                                                              cfg.CreateMap<User, UserDTO>();
                                                          }).CreateMapper());

            return collectionService;
        }
    }
}
