﻿using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Tests.Fakes;
using DataAccessLayer.Repositories.Interfaces;
using DataAccessLayer.Models;
using DataAccessLayer.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace BusinessLogicLayer.Tests
{
    public class TeamServiceTest
    {
        readonly IRepository<Team> _teamRep;
        readonly IRepository<User> _userRep;

        public TeamServiceTest()
        {
            var context = new HomeworkDbContextFake();
            var unitOfWork = new UnitOfWork(context);

            _teamRep = new RepositoryFake<Team>(context, unitOfWork);
            _userRep = new RepositoryFake<User>(context, unitOfWork);
        }

        [Fact]
        public void AddUserToTeam_CheckIfSuccessfull()
        {
            var team = new Team
            {
                Name = "Test team name"
            };

            _teamRep.Create(team);
            _teamRep.unitOfWork.SaveChanges();

            var user = new User
            {
                FirstName = "Anton",
                LastName = "Matsyshyn",
                Email = "anton.matsyshyn@outlook.com",
            };

            _userRep.Create(user);
            _userRep.unitOfWork.SaveChanges();

            user.TeamId = team.Id;

            _userRep.Update(user);
            _userRep.unitOfWork.SaveChanges();

            Assert.Equal(team.Id, _userRep.Get(user.Id).TeamId);
        }
    }
}
