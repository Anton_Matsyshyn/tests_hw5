﻿using Client.Models.ApiModels;
using Client.Services;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;
using System.Linq;
using ApplicationTask = Client.Models.ApiModels.Task;
using ThreadTask = System.Threading.Tasks.Task;

namespace Client
{
    class Program
    {
        static async ThreadTask Main(string[] args)
        {
            var configuration = new ConfigurationBuilder()
            .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
            .AddJsonFile("appsettings.json", false)
            .Build();

            var collectionService = new CollectionService(configuration);

            var projectOwnerId = 1;
            Console.WriteLine("Task №1");
            Console.WriteLine($"User id: {projectOwnerId}");
            Console.WriteLine("Answer:");

            //1st task
            //Отримати кількість тасків у проекті конкретного користувача...
            var dictionary = await collectionService.GetProjectsIdAndTaskCount(projectOwnerId);
            foreach (var item in dictionary)
            {
                Console.WriteLine($"{item.Key} - {item.Value}");
            }

            Console.WriteLine();
            Console.WriteLine();

            var userId_2ndTask = 2;
            Console.WriteLine("Task №2");
            Console.WriteLine($"User id: {userId_2ndTask}");
            Console.WriteLine("Answer (list of user's tasks id):");

            //2nd task
            //Отримати список тасків, призначених для конкретного користувача...
            var userTaskCollection = await collectionService.GetUsersTask(userId_2ndTask);
            foreach (var item in userTaskCollection)
            {
                Console.WriteLine(item.Id);
            }

            Console.WriteLine();
            Console.WriteLine();

            var userId_3dTask = 4;
            Console.WriteLine("Task №3");
            Console.WriteLine($"User id: {userId_3dTask}");
            Console.WriteLine("Answer (task id - task name):");

            //3d task
            //Oтримати список (id, name) з колекції тасків, які виконані...
            var finishedUserTasks = await collectionService.GetFinishedUserTasks(userId_3dTask);
            foreach (var item in finishedUserTasks)
            {
                Console.WriteLine($"{item.TaskId} - {item.TaskName}");
            }

            Console.WriteLine();
            Console.WriteLine();

            Console.WriteLine("Task №4");
            Console.WriteLine("Answer (team id - team name - user id)");
            //4th task
            //Отримати список (id, ім'я команди і список користувачів) з колекції команд...
            var teamWithUsers = await collectionService.GetTeamsWithUsers();
            foreach (var team in teamWithUsers)
            {
                foreach (var user in team.Members)
                {
                    Console.WriteLine($"{team.TeamId} - {team.TeamName} - {user.Id}");
                }
                if (team.Members != null && team.Members.Count() != 0)
                    Console.WriteLine();
            }

            Console.WriteLine();

            Console.WriteLine("Task №5");
            Console.WriteLine("Answer (user's first name - task name):");
            //5th task
            //Отримати список користувачів за алфавітом first_name...
            var usersWithTask = await collectionService.GetUsersWithTasks();
            foreach (var user in usersWithTask)
            {
                Console.WriteLine($"{user.User.FirstName}'s tasks:");
                if (user.Tasks != null && user.Tasks.Count() != 0)
                {
                    foreach (var task in user.Tasks)
                    {
                        Console.WriteLine($"{task.Name}");
                    }
                }
                else
                {
                    Console.WriteLine("No tasks");
                }
                Console.WriteLine();
            }

            Console.WriteLine();

            Console.WriteLine("Task №6");
            Console.WriteLine("Answer (project author id - last project id - task count - unfinished tasks - longest task id)");
            //6th task
            //Отримати наступну структуру (передати Id користувача в параметри)...
            var lastProject = await collectionService.GetLastUserProject(7);
            Console.WriteLine($"{lastProject.Author?.Id} - {lastProject.LastProject?.Id} - {lastProject.TaskCount} - {lastProject.UnfinishedOrCanceledTaskCount} - {lastProject.LongestTask?.Id}");

            Console.WriteLine();
            Console.WriteLine();

            Console.WriteLine("Task №7");
            Console.WriteLine("Answer (project id - longest task id (if exist) - shortest task id (if exist) - teammates count):");
            //7th task
            //Отримати таку структуру...
            var projectsWithTeams = await collectionService.GetProjectWithTeam();
            foreach (var projectWithTeam in projectsWithTeams)
            {
                Console.WriteLine($"{projectWithTeam.Project.Id} - {projectWithTeam.LongestTask?.Id} - {projectWithTeam.ShortestTask?.Id} - {projectWithTeam.TeammatesCount}");
            }

            Console.ReadKey();
        }
    }
}
