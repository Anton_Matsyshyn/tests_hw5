﻿
namespace Client.Models.ApiModels.CollectionModels
{
    public class LastProjectAndTaskInfo
    {
        public User Author { get; set; }
        public Project LastProject { get; set; }
        public int TaskCount { get; set; }
        public int UnfinishedOrCanceledTaskCount { get; set; }
        public Task LongestTask { get; set; }
    }
}
