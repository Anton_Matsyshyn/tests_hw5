﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Client.Models.ApiModels.CollectionModels
{
    public class KeyValue
    {
        public int Key { get; set; }
        public int Value { get; set; }
    }
}
